//
//  Pokemon.swift
//  ios-app
//
//  Created by Francisco Izurieta on 6/4/19.
//  Copyright © 2019 Francisco Izurieta. All rights reserved.
//

import Foundation
import ObjectMapper

class PokemonResponse: Mappable {
    var pokemon: [Pokemon]?
    var nextUrl: String?
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        pokemon <- map["results"]
        nextUrl <- map["next"]
    }
}

class Pokemon: Mappable {
    var name: String?
    var height: Double?
    var weight: Double?
    var imageURL: String?
    var types: [PokemonType]?
    var url: String?
    var pokemonId: Int?
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        name <- map["name"]
        height <- map["height"]
        weight <- map["weight"]
        imageURL <- map["sprites.front_default"]
        types <- map["types"]
        url <- map["url"]
        pokemonId <- map["id"]
    }
}

class PokemonType: Mappable {
    var name: String?
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        name <- map["type.name"]
    }
}

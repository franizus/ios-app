//
//  FavouritesViewController.swift
//  ios-app
//
//  Created by Francisco Izurieta on 6/21/19.
//  Copyright © 2019 Francisco Izurieta. All rights reserved.
//

import UIKit
import RealmSwift

class FavouritesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var favouritesTableView: UITableView!
    
    var pokemon: [PokemonEntity] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let realm = try! Realm()
        pokemon = Array(realm.objects(PokemonEntity.self).filter("isFavourite = true"))
        self.favouritesTableView.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pokemon.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pokemonCellFav") as! PokemonTableViewCell
        
        cell.pokemonNameLabel.text = pokemon[indexPath.row].name.capitalized
        
        cell.pokemonImageView.kf.setImage(with: URL(string: pokemon[indexPath.row].imageURL))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "toPokemonFavSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toPokemonFavSegue" {
            let destination = segue.destination as! PokemonViewController
            let selectedPokemon = pokemon[favouritesTableView.indexPathForSelectedRow?.row ?? 0]
            
            destination.pokemonURL = "https://pokeapi.co/api/v2/pokemon/\(selectedPokemon.pokemonId)"
            destination.pokemonName = selectedPokemon.name
        }
    }
}

//
//  SettingsViewController.swift
//  ios-app
//
//  Created by Francisco Izurieta on 6/14/19.
//  Copyright © 2019 Francisco Izurieta. All rights reserved.
//

import UIKit
import FirebaseAuth

class SettingsViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func singOutButtonPressed(_ sender: Any) {
        do {
            try Auth.auth().signOut()
            dismiss(animated: true, completion: nil)
        } catch {
            
        }
    }
}

//
//  PokemonEntity.swift
//  ios-app
//
//  Created by Francisco Izurieta on 6/18/19.
//  Copyright © 2019 Francisco Izurieta. All rights reserved.
//

import Foundation
import RealmSwift

class PokemonEntity: Object {
    @objc dynamic var pokemonId: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var height: Double = 0
    @objc dynamic var weight: Double = 0
    @objc dynamic var imageURL: String = ""
    @objc dynamic var isFavourite: Bool = false
    
    convenience init(pokemon: Pokemon) {
        self.init()
        pokemonId = pokemon.pokemonId!
        name = pokemon.name!
        height = pokemon.height!
        weight = pokemon.weight!
        imageURL = pokemon.imageURL!
    }
    
    override static func primaryKey() -> String? {
        return "pokemonId"
    }
}

//
//  PokemonViewController.swift
//  ios-app
//
//  Created by Francisco Izurieta on 6/4/19.
//  Copyright © 2019 Francisco Izurieta. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import Kingfisher
import RealmSwift

class PokemonViewController: UIViewController, UITableViewDataSource {
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var pokemonImageView: UIImageView!
    @IBOutlet weak var typesTableView: UITableView!
    
    var pokemonName: String?
    var pokemonURL: String = ""
    var types: [PokemonType] = []
    var pokemonId: Int!
    var pokemon: Pokemon?
    var favButtonAction: String = "Add"
    var pokemonDB: PokemonEntity? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = pokemonName?.capitalized ?? "Pokemon"
        downloadPokemonInfo()
        
        let realm = try! Realm()
        let pokemon = Array(realm.objects(PokemonEntity.self).filter("isFavourite = true"))
        
        if let pok = pokemon.first(where: {$0.name == pokemonName}) {
            pokemonDB = pok
            favButtonAction = "Remove"
        }
        
        let button = UIBarButtonItem(title: favButtonAction, style: .done, target: self, action: #selector(favTapped))
        self.navigationItem.rightBarButtonItem = button
    }
    
    @objc func favTapped(){
        let userDefaults = UserDefaults.standard
        var favouritePokemon = userDefaults.array(forKey: "favPokemon") ?? []
        favouritePokemon.append(pokemonId)
        userDefaults.set(favouritePokemon, forKey: "favPokemon")

        let realm = try! Realm()
        if pokemonDB == nil {
            try! realm.write {
                let pokemonEntity = PokemonEntity(pokemon: pokemon!)
                pokemonEntity.isFavourite = true
                realm.add(pokemonEntity, update: true)
            }
        } else {
            try! realm.write {
                pokemonDB?.isFavourite = false
            }
        }
        
        performSegueToReturnBack()
    }
    
    func performSegueToReturnBack()  {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func downloadPokemonInfo() {
        Alamofire.request(pokemonURL).responseObject { (response: DataResponse<Pokemon>) in
            self.pokemon = response.value
            self.heightLabel.text = "\(response.value?.height ?? 0)"
            self.weightLabel.text = "\(response.value?.weight ?? 0)"
            self.types = response.value?.types ?? []
            self.pokemonId = response.value?.pokemonId
            self.typesTableView.reloadData()
            self.pokemonImageView.kf.setImage(with: URL(string: (response.value?.imageURL ?? "https://i.ebayimg.com/images/g/huAAAOSwT6pV2hB6/s-l300.jpg")))
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return types.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = types[indexPath.row].name ?? "n/a"
        return cell
    }
}

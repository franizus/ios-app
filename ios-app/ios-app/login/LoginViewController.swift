//
//  LoginViewController.swift
//  ios-app
//
//  Created by Francisco Izurieta on 4/16/19.
//  Copyright © 2019 Francisco Izurieta. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore

class LoginViewController: UIViewController {
    @IBOutlet weak var loginImageView: UIImageView!
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //loginImageView.image = UIImage(named: "Image")
        //loginImageView.contentMode = .scaleToFill
    }
    
    override func viewWillAppear(_ animated: Bool) {
        checkRegistration()
    }
    
    func checkRegistration() {
        let db = Firestore.firestore()
        
        guard let currentUser = Auth.auth().currentUser else {
            return
        }
        
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
        
        let userRef = db.collection("users").document(currentUser.uid)
        userRef.getDocument { (snapshot, error) in
            self.activityIndicator.stopAnimating()
            if error != nil || snapshot?.get("registerCompleted") == nil {
                self.performSegue(withIdentifier: "toRegisterSegue", sender: self)
            } else {
                self.performSegue(withIdentifier: "toMainSegue", sender: self)
            }
        }
    }
    
    @IBAction func loginButtonPressed(_ sender: Any, forEvent event: UIEvent) {
        /*let mail = "f"
        let password = "a"
        
        if (mail, password) == (loginTextField.text, passwordTextField.text) {
            performSegue(withIdentifier: "toMainView", sender: self)
            return
        }
        
        showAlertError()*/
        
        firebaseAuth(email: loginTextField.text!, passwd: passwordTextField.text!)
    }
    
    func firebaseAuth(email:String, passwd:String) {
        Auth.auth().signIn(withEmail: email, password: passwd) { (result, error) in
            if let _ = error {
                self.showAlertError()
                return
            }
            self.checkRegistration()
        }
    }
    
    @IBAction func createAccountButtonPressed(_ sender: Any, forEvent event: UIEvent) {
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        loginTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }
    
    func showAlertError() {
        let alertView = UIAlertController(title: "Error", message: "Please enter valid credentials.", preferredStyle: .alert)
        
        let okAlertAction = UIAlertAction(title: "Ok", style: .default) { (_) in
            self.loginTextField.text = ""
            self.passwordTextField.text = ""
        }
        
        alertView.addAction(okAlertAction)
        
        present(alertView, animated: true, completion: nil)
    }
    
    /*func operation(_ n1: Int, _ n2: Int) -> Int {
        return n1 + n2
    }
    
    func handleOperation(n1: Int, n2: Int, operation: (Int, Int)->Int) {
        print(operation(n1, n2))
    }*/
}
